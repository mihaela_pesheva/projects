def menu():
    
    print('\tHello to the Calculator App.')
    print('\tPlease, choose an operation from the menu:')
    print('1. Addition +')
    print('2. Substraction -')
    print('3. Multiplication ×')
    print('4. Division ÷')
    print('5. Find Square Root of a number')
    print('6. Exponentiation')
    print('7. Exit')
    
    while True: 
        try:
            choice = int(input('\n\nPlease enter your choice: '))
            if choice == 1:
                addition()
            elif choice == 2:
                substraction()
            elif choice == 3:
                multiplication()
            elif choice == 4:
                division()
            elif choice == 5:
                square_root()
            elif choice == 6:
                exponentiation()
            elif choice == 7:
                print('\nGoodbye!')
                break
            else:
                print('Please enter a valid value from the list.')
        except ValueError:
            print('Please enter a valid value from the list.')

def addition():
    a = get_number()
    print('\t\t\t+')
    b = get_number()
    print('Answer: {}'.format(a+b))
    
def substraction():
    a = get_number()
    print('\t\t\t-')
    b = get_number()
    print('Answer: {}'.format(a-b))
       
def multiplication():
    a = get_number()
    print('\t\t\t×')
    b = get_number()
    print('Answer: {}'.format(a*b))

def division():
    a = get_number()
    print('\t\t\t÷')
    b = get_number()
    print('Answer: {}'.format(a/b))

def square_root():
    a = get_number()
    print('Square root is: {}'.format(a**0.5))
    
def exponentiation():
    a = get_number()
    b = get_number()
    print('Answer: {}'.format(a**b))

def get_number():
    while True:
        try:
            a = int(input('Please enter a number: '))
            break
        except ValueError:
            print('Please enter a valid integer/float value.')
    return a




if __name__ == '__main__':
    menu()