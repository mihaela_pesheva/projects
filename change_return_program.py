def returnChange():
    while True:
        try:
            money_given = int(input('Please enter your initial money: '))
            break
        except ValueError:
            print('Please enter a valid integer value')
    while True:
        try:
            cost = int(input('Please enter the price of the item: '))
            break
        except ValueError:
            print('Please enter a valid integer value')
            


    change = money_given - cost
    
    amount_quarters = int(change/25)
    left_quarters = int(change%25)
    amount_dimes = int(left_quarters/10)
    left_dimes = int (left_quarters%10)
    amount_nickels = int(left_dimes/5)
    left_nickels = int(left_dimes%5)
    amount_pennies = int(left_nickels/1)
    left_pennies = int(left_nickels%1)

    answer = 'The change is : {} quarters, {} dimes, {} nickels and {} pennies'.format(amount_quarters, amount_dimes, amount_nickels, amount_pennies)
    print(answer)
    return answer

if __name__ == "__main__":
    returnChange()
    
