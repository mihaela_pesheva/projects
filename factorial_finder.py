
def find_fact():

	while True:
		try:
			n = int(input("Enter a positive integer: "))
			break
		except ValueError:
			print('Please enter a valid integer value')
    
	print("Factorial of {} is {}".format(n, multiply(n)))
    

def multiply(n):

    if (n >= 1):
        return n * multiply(n-1)
    else:
        return 1
if __name__ == '__main__':
	find_fact()