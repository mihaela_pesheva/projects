def prime_factorization():

    while True:
        try:
            n = int(input('Please enter a number: '))
            break
        except:
            print('Please enter a valid integer value')

    while n%2 == 0:
        print(2)
        n = n / 2

    i = 3

    for i in range(3, i**int(n)):

        while (n%i == 0):
            print(i)
            n = n / i
            i = i + 2

    if (n > 2):
        print(n)

if __name__ == '__main__':
    prime_factorization()