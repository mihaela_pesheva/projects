YES_ANSWER = 'yes'
NO_ANSWER = 'no'

def gen_primes():
    
    D = {}
    q = 2
    
    while True:
        if q not in D:
            yield q
            D[q * q] = [q]
        else:
            for p in D[q]:
                D.setdefault(p + q, []).append(p)
            del D[q]
        
        q += 1

def main():

    prime = gen_primes()
    while True:
        answer = input('\nDo you want to see the next prime number? (Yes or No)\t')
        answer_lower = answer.lower()

        if answer_lower == YES_ANSWER:
            print(next(prime))
        elif answer_lower == NO_ANSWER:
            print('Goodbye')
            break
        else:
            print('Please answer with yes or no')

            
if __name__ == '__main__':
    main()