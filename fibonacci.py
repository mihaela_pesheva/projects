
def fib():
    
   while True:
        try:
            n = int(input('Please enter range: '))
            break

        except ValueError as ex:
            print('Please enter a valid integer value.')


    first,second = 0, 1
    fib = []
    fib.append(first)
    fib.append(second)
    
    while(n != 2):
        third = first + second
        first = second
        second = third
        fib.append(third)
        n = n - 1
    print(fib)
    return fib


if __name__ == "__main__":
    fib()

    