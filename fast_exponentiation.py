def fast_exponentiation():

	while True:
		try:
			first_digit = float(input('Please enter the first digit: '))
			break
		except ValueErorr:
			print('Please enter a valid integer value.')

	while True:
		try:
			second_digit = float(input('Please enter the second digit: '))
			break
		except ValueError:
			print('Please enter a valid integer value.')

    print('Answer is: {}'.format(first_digit**second_digit))


if __name__ == '__main__':
	fast_exponentiation()