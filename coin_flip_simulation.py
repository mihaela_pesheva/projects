YES_ANSWER = 'yes'
NO_ANSWER = 'no'

import random
def flip():

    play = True
    num_t = 0
    num_h = 0

    while play:
        answer = input('\nDo you want to flip the coin? (Yes or No)\t')
        answer_lower = answer.lower()

        if answer_lower == YES_ANSWER:
            sides = ['Tails', 'Heads']            
            luck = random.choice(sides)
            print('\n' + luck)

            if luck == 'Tails':
                num_t += 1
            else:
                num_h += 1
            
        elif answer_lower == NO_ANSWER:
            print('number of tails: {}'.format(num_t))
            print('number of heads: {}'.format(num_h))
            print('Goodbye then!')
            break
            
        else:
            print('Please answer with Yes or No')

if __name__ == "__main__":
    flip()
    

