def make_pi():
    q, r, t, k, m, x = 1, 0, 1, 1, 3, 3

    for j in range(1000):
        if (4 * q + r - t) < (m * t):
            yield m
            q, r, t, k, m, x = 10 * q, 10*(r-m*t), t, k, (10*(3*q+r))//t - 10 * m, x
        else:
            q, r, t, k, m, x = q * k, (2*q+r)*x, t*x, k+1, (q*(7*k+2)+r*x)//(t*x), x + 2

def show_pi():

    my_numbers = make_pi()
    while True:
        try:
            answer = int(input('Please enter a range: '))
            break
        except ValueError:
            print('Please enter a valid integer value.')
    
    for i in range(answer):
        if (i == 1):
            print('.')
        print(next(my_numbers))

if __name__ == '__main__':    
    show_pi()